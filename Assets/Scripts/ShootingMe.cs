﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootingMe : MonoBehaviour
{
    public GameObject UI;

    public GameObject directionref;
    public GameObject bullet;
    public float speed = 4;
    public PlayerMovement pm;

    public int ammo = 6;
    public float cooldown = 0f;
    public Text ammoText;

    public string text;
    public bool done;
    public GameObject hsLabel;
    public AudioSource fire;
    public AudioSource empty;
    public AudioSource reload;

    private bool isPaused = false;

    void Start()
    {
        pm = GetComponentInParent<PlayerMovement>();
        hsLabel = GameObject.Find("Canvas/ReloadPanel");
        ammoText = GameObject.Find("Canvas/Ammo/Text").GetComponent<Text>();

        hsLabel.SetActive(false);

        fire = GetComponent<AudioSource>();
    }

    void Update()
    {
        ammoText.text = "Ammo: " + ammo.ToString();
        float dirr = Input.GetAxis("Vertical");
        float dir = Input.GetAxis("Horizontal");
        if(cooldown >= 0)
        {
            cooldown -= 0.25f;
        }

        if (Time.timeScale == 1f)
        {
            isPaused = true;
        }
        else
        {
            isPaused = false;
        }

        if(isPaused)
        {
            if (ammo > 0)
            {
                if (Input.GetMouseButtonDown(0) && cooldown <= 0)
                {
                    cooldown = 3f;
                    GameObject p = Instantiate(bullet, directionref.transform.position, directionref.transform.rotation);
                    var bullett = p.GetComponent<Rigidbody2D>();

                    bullett.velocity = pm.currentdir * speed;

                    ammo--;
                    fire.Play(0);
                    //Debug.Log("Cuck ammo");
                }
            }

            if (ammo == 0)
            {
                //Debug.Log("cuck ammo 2");
                hsLabel.SetActive(true);

                if ((Input.GetKeyDown(KeyCode.R)))
                {
                    ammo += 6;
                    reload.Play();
                    hsLabel.SetActive(false);
                }
                else if (Input.GetMouseButtonDown(0))
                {
                    empty.Play();
                }
            }
        }
    }
}