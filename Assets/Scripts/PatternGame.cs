﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternGame : MonoBehaviour
{
    private string passkey = "";
    private string userkey = "";
    private int index = 0;
    private bool _interact;

    public GameObject player;
    public GameObject lockedDoor;
    public GameObject cPanel;
    public Animator keyInfo;

    public AudioSource correctBeep;
    public AudioSource wrongBeep;
    public AudioSource winBeep;

    void Start()
    {
        randomizeKey();
        Debug.Log(passkey);
        cPanel.SetActive(false);
        lockedDoor.SetActive(false);
    }

    void Update()
    {
        _interact = Input.GetKeyDown(KeyCode.E);
    }

    void randomizeKey()
    {
        index++;
        passkey += Random.Range(1, 4).ToString();
        if (index < 4)
        {
            randomizeKey();
        }
        else
        {
            checkKey();
        }
    }

    public void ButtonA()
    {
        userkey += 1.ToString();
        checkKey();
    }

    public void ButtonB()
    {
        userkey += 2.ToString();
        checkKey();
    }

    public void ButtonC()
    {
        userkey += 3.ToString();
        checkKey();
    }

    public void ButtonD()
    {
        userkey += 4.ToString();
        checkKey();
    }

    public void goBack()
    {
        player.SetActive(true);
        cPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    void checkKey()
    {
        if (userkey != passkey.Substring(0, index))
        {
            wrongBeep.Play();
            keyInfo.SetInteger("userKeyPress", 2);
            Debug.Log("Wrong Input");
            userkey = "";
            index = 0;
        }
        else if (userkey == passkey)
        {
            winBeep.Play();
            keyInfo.SetInteger("userKeyPress", 1);
            Debug.Log("Unlocked!");
            lockedDoor.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Invoke("win", 3);
        }
        else
        {
            correctBeep.Play();
            keyInfo.SetInteger("userKeyPress", 1);
        }

        index++;
    }

    void resetKey()
    {
        keyInfo.SetInteger("userKeyPress", 0);
    }

    void win()
    {
        gameObject.SetActive(false);
        player.SetActive(true);
        lockedDoor.SetActive(true);
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (_interact)
            {
                cPanel.SetActive(true);
                player.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}
