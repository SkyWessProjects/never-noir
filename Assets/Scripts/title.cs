﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class title : MonoBehaviour
{

    GameObject obj;
    public AudioClip mainTrack;
    private AudioManager theAM;

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        theAM = FindObjectOfType<GameManager>().GetComponent<AudioManager>();
        theAM.ChangeBGM(mainTrack);
    }

    public void Play()
    {
        SceneManager.LoadScene("FactoryExterior");
    }

    public void Quit()
    {
        Application.Quit();        
    }

}