﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private static bool created = false;
    public static int curScene = 0;
    public static int prevScene = 0;
    public AudioSource curSong;

    public GameObject player;
    static public GameManager Get()
    {
        return instance;
    }

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void LateUpdate()
    {
        prevScene = SceneManager.GetActiveScene().buildIndex;
    }

    public void setPlayer()
    {
        curScene = SceneManager.GetActiveScene().buildIndex;
        //Debug.Log(curScene);
        //Debug.Log(prevScene);
        player = GameObject.FindGameObjectWithTag("Player");
        if(prevScene == 2)
        {
            player.transform.position = new Vector3(92, -30, 0);
        }
        else if(prevScene == 3 && curScene == 1)
        {
            player.transform.position = new Vector3(12, -20, 0);
        }
        else if(prevScene == 6)
        {

        }
    }
}
