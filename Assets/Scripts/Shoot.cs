﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour


{
    public float FireRate = 0;
    public float Damage = 10;
    public LayerMask notToHit;

    float TimeToFire = 0;
    Transform FirePoint;


    // Start is called before the first frame update
    void Awake()
    {
        FirePoint = transform.Find ("FirePoint");
        if (FirePoint == null)
        {
            Debug.Log("No FirePoint");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(FireRate == 0)
        {
            if(Input.GetKeyDown(KeyCode.Mouse1)){
                shoot();
            }
        }

        else {

            if (Input.GetButton("Fire1") && Time.time < TimeToFire)
            {
                TimeToFire = Time.time + 1 / FireRate;
                shoot();
            }
        }
    }
    void shoot()
    {
        Debug.Log("Fire!");
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition) .x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 FirePointPosition = new Vector2(FirePoint.position.x, FirePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(FirePointPosition, mousePosition - FirePointPosition, 100, notToHit);
        Debug.DrawLine(FirePointPosition, mousePosition);
    }
}

