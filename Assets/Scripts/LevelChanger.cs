﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    //grabs what level unity tells it to load
    public string levelToLoad;
    public bool _change = false;

    void Update()
    {
        _change = Input.GetKeyDown(KeyCode.E);
        //Debug.Log(_change);
    }

    public void changeLevel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(levelToLoad);
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if(_change)
            {
                changeLevel();
            }
        }
    }
}
