﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monsterBreak : MonoBehaviour
{
    public GameObject hole;
    public GameObject exit;
    public Animator animator;
    public AudioClip ambientTrack;

    private AudioManager theAM;

    void Awake()
    {
        hole.SetActive(false);
        exit.SetActive(false);
        animator.enabled = false;
        theAM = FindObjectOfType<GameManager>().GetComponent<AudioManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            hole.SetActive(true);
            exit.SetActive(true);
            animator.enabled = true;
            theAM.ChangeBGM(ambientTrack);
        }
    }
}
