﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHider : MonoBehaviour
{
    public Text hsLabel;

    public GameObject UI;
    public GameObject popup;
    public GameObject exitPopup;

    private bool _interact = false;

    void Start()
    {
        UI.SetActive(false);
        popup.SetActive(false);
        exitPopup.SetActive(false);
    }

    private void Update()
    {
        _interact = Input.GetKeyDown(KeyCode.E);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Interactable"))
        {
            popup.SetActive(true);
        }
        else if(collider.CompareTag("Exit"))
        {
            exitPopup.SetActive(true); 
        }
        else if(collider.CompareTag("Exit2"))
        {
            exitPopup.SetActive(true);
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.CompareTag("Interactable"))
        {
            if (_interact)
            {
                Interactable interactable = collider.GetComponent<Interactable>();
                if(interactable != null)
                {
                    interactable.done = true;
                    hsLabel.text = interactable.text;
                    UI.SetActive(true);
                    popup.SetActive(false);
                }
            }            
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.CompareTag("Interactable"))
        {
            UI.SetActive(false);
            popup.SetActive(false);
        }
        else if(collider.CompareTag("Exit"))
        {
            exitPopup.SetActive(false);
        }
        else if(collider.CompareTag("Exit2"))
        {
            exitPopup.SetActive(false);
        }
    }
}