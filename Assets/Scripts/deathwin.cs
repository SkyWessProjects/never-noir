﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class deathwin : MonoBehaviour
{
    //This takes the script that is inside of a gameobject
    public PlayerHealth phealth;
    public GameObject deathPanel;

    void Awake()
    {
        deathPanel = GameObject.Find("Canvas/Death");
        //This is how you find those gameobjects with the specified script
        phealth = GameObject.Find("Player_character").GetComponent<PlayerHealth>();
        deathPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //checking to see if player or monster is dead and winning or losing accordingly
        if(phealth.CurrentHealth <= 0)
        {
            deathPanel.SetActive(true);
            Invoke("lose", 3);
        }
    }

    void lose()
    {
        SceneManager.LoadScene("Title");
    }
}
