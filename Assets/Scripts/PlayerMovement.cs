﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [HideInInspector]
    public Vector2 currentdir = Vector2.down;
    public Vector2 currentdirr = Vector2.up;
    public float speed; //Controls speed. In Unity, we can now do the thing in the inspector to change it.
                        // Start is called before the first frame update; This script is called once when the object starts
    public Animator animator;
    public ShootingMe sm;

    private bool isfacingright = true;

    void Start()//Defined as a function

    {
        sm = GetComponentInParent<ShootingMe>();
        Cursor.lockState = CursorLockMode.Locked;
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.setPlayer();
    }

    // Update is called once per frame; This script is constantly ran/checked after start. Basically a loop.
    void Update() //Defined as a function
    {
        float verticalMovement = Input.GetAxis("Vertical");
        float horizontalMovement = Input.GetAxis("Horizontal");

        animator.SetFloat("horizontal", (horizontalMovement));
        animator.SetFloat("vertical", (verticalMovement));

        if (horizontalMovement > 0 || verticalMovement > 0 || horizontalMovement < 0 || verticalMovement < 0)
        {
            currentdir = Vector2.right * horizontalMovement + Vector2.up * verticalMovement;
            currentdir.Normalize();
        }
        animator.SetFloat("currentdirX", (currentdir.x));

        if (Input.GetAxis("Horizontal") < 0f && isfacingright )
        {
            var tarScale = transform.localScale;
            tarScale.x = -Mathf.Abs(tarScale.x);
            transform.localScale = tarScale;
            sm.directionref.transform.Rotate(0f, 180f, 0f);
            isfacingright = !isfacingright;
            
        }
        else if (Input.GetAxis("Horizontal") > 0f && !isfacingright)
        {
            var tarScale = transform.localScale;
            tarScale.x = Mathf.Abs(tarScale.x);
            transform.localScale = tarScale;
            sm.directionref.transform.Rotate(0f, 180f, 0f);
            isfacingright = !isfacingright;
            //Debug.LogWarning("Change");
        }
    }

    void FixedUpdate()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        transform.Translate((Vector3.right * speed) * x);
        transform.Translate((Vector3.up * speed) * y);
    }
}


  