﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public float CurrentHealth { get; set; }
    public float MaxHealth { get; set; }

    public PlayerMovement movement;
    public Slider healthbar;
    public Animator animator;
    public AudioSource swing;
    void Start()
    {
        MaxHealth = 100f;
        CurrentHealth = MaxHealth;

        healthbar.value = MaxHealth;
        movement = GameObject.Find("Player_character").GetComponent<PlayerMovement>();
    }

    void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.X))
        {
            DealDamage(10);
            animator.SetBool("Ishit", true);
        }
        */
    }

    public void DealDamage(float damageValue)
    {
        CurrentHealth -= damageValue;
        healthbar.value -= damageValue;

        animator.SetTrigger("Ishit");

        if (CurrentHealth <= 0)
        {
            movement.GetComponent<PlayerMovement>();
            movement.enabled = false;
            Invoke("Die", 1);
        }

    }
    float CalculatedHealth()
    {
        return CurrentHealth / MaxHealth;

    }

    void Die()
    {
        CurrentHealth = 0;
        SceneManager.LoadScene("Title");
        Debug.Log("You ded");
        Destroy(gameObject);
    }

    public void OnCollisionStay2D(Collision2D Other)
    {
        //Handles the melee attack
        if (Other.gameObject.tag == "Monster" && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            var monster = Other.gameObject.GetComponent<MonsterHealth>();
            monster.DealDamage(20);
            swing.Play();
        }
    }
}