﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectiveVision : MonoBehaviour
{
    //sets up components that will be used
    public GameObject highlighted;
    public Slider detectiveSlider;
    public GameObject shadow;
    private bool canUseAbility = true;
    private bool toggle = false;

    public Animator anim;

    void Start()
    {
        //finds components and setsthem to false
        highlighted = GameObject.Find("DetectiveVision");
        shadow = GameObject.Find("Canvas/DetectiveShadow");
        detectiveSlider = GameObject.Find("Canvas/PlayerGauge/DetectiveGauges").GetComponent<Slider>();

        highlighted.SetActive(false);
        shadow.SetActive(false);
        //anim = shadow.GetComponent<Animator>;
    }

    // Update is called once per frame
    void Update()
    {
        //This checks to see if the player can use detective vision or not and then enables it
        if (Input.GetKeyDown(KeyCode.Tab) && canUseAbility)
        {
            toggle = !toggle;
        }

        if (toggle)
        {
            //Debug.Log("Toggled ON");
            //This stuff messes with the detective gauge
            if (detectiveSlider.value > 0.5f)
            {
                detectiveSlider.value -= 0.01f;
                highlighted.SetActive(true);
                shadow.SetActive(true);
            }
            else if (detectiveSlider.value != 10f)
            {
                toggle = !toggle;
                
                highlighted.SetActive(false);
                shadow.SetActive(false);   
            }

        }
        else
        {
            //Debug.Log("Toggled OFF");
            detectiveSlider.value += 0.025f;
            if (detectiveSlider.value < 1f)
            {
                canUseAbility = false;
            }
            else if (detectiveSlider.value > 3f)
            {
                canUseAbility = true;
            }

            highlighted.SetActive(false);
            shadow.SetActive(false);
        }


    }
}
