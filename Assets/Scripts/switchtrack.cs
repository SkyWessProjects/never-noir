﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchtrack : MonoBehaviour
{
    public AudioClip track;

    private AudioManager theAM;

    // Start is called before the first frame update
    void Start()
    {
        theAM = FindObjectOfType<GameManager>().GetComponent<AudioManager>();
        theAM.ChangeBGM(track);
    }
}
