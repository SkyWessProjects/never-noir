﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Handboi : MonoBehaviour
{
    public float CurrentHealth { get; set; }
    public float MaxHealth { get; set; }

    public Slider healthbar;


    public float speed = 2f;
    public Transform Player;

    public float stopdistance = 0.5f;

    private float lasthit = 0;
    public float hitintv = 1;

    private float llasthit = 0;
    public float hhitintv = 1;

    public Animator animator;

    private bool isfacingright = true;
    public GameObject directionref;

    public AudioSource Monsterhit;
    public AudioSource Monsterded;
    public AudioSource Playerhit;

    void Start()
    {
        MaxHealth = 200f;
        CurrentHealth = MaxHealth;

        healthbar.value = CurrentHealth;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            DealDamage(30);

        }

        Vector3 displacement = Player.position - transform.position;
        displacement = displacement.normalized;
        if (Vector2.Distance(Player.position, transform.position) > stopdistance)
        {
            transform.position += (displacement * speed * Time.deltaTime);

        }


        if (displacement.x < 0 && isfacingright)
        {
            var tarScale = transform.localScale;
            tarScale.x = Mathf.Abs(tarScale.x);
            transform.localScale = tarScale;
            directionref.transform.Rotate(0f, 180f, 0f);
            isfacingright = !isfacingright;


        }

        else if (displacement.x > 0 && !isfacingright)
        {
            var tarScale = transform.localScale;
            tarScale.x = -Mathf.Abs(tarScale.x);
            transform.localScale = tarScale;
            directionref.transform.Rotate(0f, 180f, 0f);
            isfacingright = !isfacingright;
            Debug.LogWarning("Change");

        }

        animator.SetFloat("horizontal", (displacement.x));
    }

    public void DealDamage(float damageValue)
    {
        CurrentHealth -= damageValue;
        healthbar.value -= damageValue;

        if (healthbar.value <= 0 || CurrentHealth <= 0)
        {
            Monsterded.Play();
            Die();
        }
    }
    float CalculatedHealth()
    {
        return CurrentHealth / MaxHealth;

    }

    void Die()
    {
        Monsterded.Play();
        CurrentHealth = 0;

        Debug.Log("he ded");
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D Other)
    {
        if (Other.gameObject.tag == "Bullet")
        {
            if (Time.time - llasthit > hhitintv)
            {
                Monsterhit.UnPause();
                Monsterhit.Play(0);

                DealDamage(30);

                llasthit = Time.time;
                Destroy(Other.gameObject);
            }

        }

        else
        {
            Monsterhit.Pause();
        }
    }


    private void OnCollisionStay2D(Collision2D Other)
    {
        if (Other.gameObject.tag == "Player")
        {
            if (Time.time - lasthit > hitintv)
            {
                speed = 0;
                Playerhit.UnPause();
                Playerhit.Play(0);

                animator.SetTrigger("IsHit");
                var player = Other.gameObject.GetComponent<PlayerHealth>();
                player.DealDamage(10);

                lasthit = Time.time;
            }

        }
        else
        {
            Playerhit.Pause();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        speed = 2f;
    }
}