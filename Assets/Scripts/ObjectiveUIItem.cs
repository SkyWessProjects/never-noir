﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveUIItem : MonoBehaviour
{
    public Interactable interactable;
    public GameObject checkbox;

    // Start is called before the first frame update
    void Start()
    {
        checkbox.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        checkbox.SetActive(interactable.done);
    }
}
